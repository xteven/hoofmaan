#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>


// define typedefs
typedef std::unordered_map<char, int> frequency_map;
typedef std::pair<char, int> frequency_pair;
typedef std::unordered_map<char, std::string> encoding_map;
typedef std::pair<char, std::string> encoding_pair;


// the struct for a node of a binary tree
struct tree_node {
    tree_node(char v, int f, tree_node * l, tree_node * r);
    char value;
    int frequency;
    tree_node * left_child;
    tree_node * right_child;
};


tree_node::tree_node(char v, const int f, tree_node * l = nullptr, tree_node * r = nullptr) {
    value = v;
    frequency = f;
    left_child = l;
    right_child = r;
}


typedef std::vector<tree_node *> tree_vector;


// returns a map of frequencies of characters extracted from a string
frequency_map get_frequencies(const std::string &input_string) {
    frequency_map frequencies;
    for (char character: input_string) {
        if (frequencies.find(character) == frequencies.end()) {
            frequencies[character] = 1;
        } else {
            frequencies[character] += 1;
        }
    }
    return frequencies;
}


// displays a map to standard output
void display_map(const frequency_map &input_map) {
    for (frequency_pair map_element: input_map) {
        std::cout << map_element.first << " : " << map_element.second << std::endl;
    }
}


// sort a vector of tree nodes
bool tree_node_compare(tree_node * first_node, tree_node * second_node) {
    if ((first_node -> frequency) == (second_node -> frequency)) {
        return (first_node -> value) < (second_node -> value);
    } else {
        return (first_node -> frequency) > (second_node -> frequency);
    }
}


// generates a vector from a map of frequencies
tree_vector generate_vector(const frequency_map &input_map) {
    tree_vector output_vector;
    for (frequency_pair map_element: input_map) {
        output_vector.push_back(new tree_node(map_element.first, map_element.second));
    }
    std::sort(output_vector.begin(), output_vector.end(), tree_node_compare);
    return output_vector;
}


// displays a vector of tree node pointers
void display_vector(const tree_vector &input_vector) {
    for (tree_node * node_pointer : input_vector) {
        std::cout << node_pointer -> value << "-" << node_pointer -> frequency << " | ";
    }
    std::cout << std::endl;
}


// deletes a tree of tree nodes
void delete_tree(tree_node * &node_pointer) {
    if (node_pointer == nullptr) return;
    delete_tree(node_pointer -> left_child);
    delete_tree(node_pointer -> right_child);
    delete node_pointer;
}


// deletes a vector of tree node pointers
void delete_vector(tree_vector &input_vector) {
    for (tree_node * node_pointer : input_vector) {
        delete_tree(node_pointer);
    }
}


// gets the binary tree from a vector of tree node pointers
tree_node * generate_tree(tree_vector &input_vector) {
    tree_node * temporary_tree1;
    tree_node * temporary_tree2;
    while (input_vector.size() > 1) {
        temporary_tree1 = input_vector.back();
        input_vector.pop_back();
        temporary_tree2 = input_vector.back();
        input_vector.pop_back();

        input_vector.push_back(new tree_node('?', (temporary_tree1 -> frequency) + (temporary_tree2 -> frequency),
                temporary_tree1, temporary_tree2));

        // I love myself some hyper inefficiency ;)
        std::sort(input_vector.begin(), input_vector.end(), tree_node_compare);
    }
    return input_vector[0];
}


// generates the table of encoding values
void generate_table(tree_node * &node_pointer, encoding_map &current_table,
        const std::string &current_string = "") {
    if (node_pointer == nullptr) return;
    if ((node_pointer -> left_child == nullptr) && (node_pointer -> right_child == nullptr)) {
        current_table[node_pointer -> value] = current_string;
    } else {
        generate_table(node_pointer -> left_child, current_table, current_string + "0");
        generate_table(node_pointer -> right_child, current_table, current_string + "1");
    }
}


// displays a map to standard output
void display_map(const encoding_map &input_map) {
    for (encoding_pair map_element : input_map) {
        std::cout << map_element.first << " : " << map_element.second << std::endl;
    }
}


// display the final map: sorting function
bool pair_compare(const encoding_pair& pair1, const encoding_pair& pair2) {
    if (pair1.second.length() == pair2.second.length()) {
        return pair1.first < pair2.first;
    } else {
        return pair1.second.length() < pair2.second.length();
    }
}


// display the final map: outputting function
void display_vector(const std::vector<encoding_pair>& input_vector) {
    for (encoding_pair encoding_pair : input_vector) {
        std::cout << encoding_pair.first << " : " << encoding_pair.second << std::endl;
    }
}


// display the final map: main function
void display_the_final_map(const encoding_map& final_map) {
    std::vector<encoding_pair> final_vector;
    final_vector.reserve(final_map.size());
    for (encoding_pair map_element : final_map) {
        final_vector.push_back(map_element);
    }
    std::sort(final_vector.begin(), final_vector.end(), pair_compare);
    display_vector(final_vector);
}


int main() {
    // get the user input
    std::string user_input;
    std::getline(std::cin, user_input);

    // get the map of frequencies
    frequency_map frequency_map = get_frequencies(user_input);
    //display_map(frequency_map);

    // generate the vector of frequencies
    tree_vector tree_vector = generate_vector(frequency_map);
    //display_vector(tree_vector);

    // generate the tree of values
    tree_node * tree = generate_tree(tree_vector);

    // generate the table of values
    encoding_map encoding_values;
    generate_table(tree, encoding_values);

    // display the final map
    std::cout << "Hash table: " << std::endl;
    //display_map(encoding_values);
    display_the_final_map(encoding_values);

    // clean up
    delete_tree(tree);
    return 0;
}
